import org.example.PhoneBookEntry;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {

    @Test
    public void testValidEntryConstructor() {
        PhoneBookEntry entry = new PhoneBookEntry("John Doe", "+1234567890");
        assertEquals("John Doe", entry.getName());
        assertEquals("+1234567890", entry.getNumber());
    }

    @Test
    public void testInvalidEntryConstructor() {
        assertThrows(IllegalArgumentException.class, () -> new PhoneBookEntry("Invalid", "123"));
    }

    @Test
    public void testEquals() {
        PhoneBookEntry entry1 = new PhoneBookEntry("John Doe", "+1234567890");
        PhoneBookEntry entry2 = new PhoneBookEntry("John Doe", "+1234567890");
        PhoneBookEntry entry3 = new PhoneBookEntry("Alice Smith", "+9876543210");

        assertEquals(entry1, entry2);
        assertNotEquals(entry1, entry3);
    }

    @Test
    public void testHashCode() {
        PhoneBookEntry entry1 = new PhoneBookEntry("John Doe", "+1234567890");
        PhoneBookEntry entry2 = new PhoneBookEntry("John Doe", "+1234567890");

        assertEquals(entry1.hashCode(), entry2.hashCode());
    }


    @Test
    public void testToString() {
        PhoneBookEntry entry = new PhoneBookEntry("John Doe", "+1234567890");
        String expected = "Name: John Doe; Nummer: +1234567890";
        assertEquals(expected, entry.toString());
    }

    @Test
    public void testCompareTo() {
        PhoneBookEntry entry1 = new PhoneBookEntry("John Doe", "+1234567890");
        PhoneBookEntry entry2 = new PhoneBookEntry("Alice Smith", "+9876543210");
        PhoneBookEntry entry3 = new PhoneBookEntry("John Doe", "+5555555555");

        assertTrue(entry1.compareTo(entry2) > 0);
        assertTrue(entry2.compareTo(entry1) < 0);


        assertTrue(entry1.compareTo(entry3) > 0);
        assertTrue(entry3.compareTo(entry1) < 0);
    }
}
