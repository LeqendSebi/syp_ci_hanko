package org.example;

import java.util.ArrayList;
import java.util.Collection;

public class Student implements Comparable {
    private String familienname;
    private String vorname;
    private int matrikelnummer;

    private static Collection<Integer> allmatrikel = new ArrayList<>();



    public Student(String familienname,String vorname,int matrikelnummer) {

        if(familienname != null && !familienname.trim().isEmpty() && vorname != null && !vorname.trim().isEmpty() && matrikelnummer > 0) {
            if(!allmatrikel.contains(matrikelnummer)) {
                allmatrikel.add(matrikelnummer);
                this.matrikelnummer = matrikelnummer;
                this.familienname = familienname;
                this.vorname = vorname;

                
            }
            else {
                throw new IllegalArgumentException("Matrikelnummer ist bereits vorhanden");
            }


        }
        else {
            throw new IllegalArgumentException("Incorrect Student");
        }

    }

    @Override
    public int compareTo(Object o) {
        if (o == null || getClass() != o.getClass()) {
            throw new IllegalArgumentException("Nicht von Typ Student");
        }

        Student otherEntry = (Student) o;

        int equal = this.familienname.compareTo(otherEntry.familienname);
        if (equal != 0) {
            return equal;
        }

        return otherEntry.vorname.compareTo(this.vorname);

    }
    @Override
    public String toString() {
        return this.familienname + "," + this.vorname + " (" + this.matrikelnummer + ")";
    }
}
