package org.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        System.out.println("PhonebookEntry: " + "\n");
        List<PhoneBookEntry> list = new ArrayList<>();
        list.add(new PhoneBookEntry("Martin", "066029556386"));
        list.add(new PhoneBookEntry("Holga", "066439285673"));
        list.add(new PhoneBookEntry("Holga", "03340289483"));
        list.add(new PhoneBookEntry("Sebastian", "044829482054"));

        System.out.println("Unsortiert: ");
        for(PhoneBookEntry entry : list) {
            System.out.println(entry);
        }
        System.out.println("Umgekehrte Reihenfolge: ");

        list.sort(Collections.reverseOrder());
        for(PhoneBookEntry entry : list) {
            System.out.println(entry);
        }

        System.out.println("Sortiert: ");
        list.sort(null);
        for(PhoneBookEntry entry : list) {
            System.out.println(entry);
        }


        System.out.println("\n" +"Student: " + "\n");
        Student s1 = new Student("Muster", "Thomas", 123456);
        Student s2 = new Student("Herbert", "Franz", 111111);
        Student s3 = new Student("Malt", "David R.", 323232);
        try {
            new Student("", "", 111111); // Exception
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        List<Student> studenten = new ArrayList<>();
        studenten.add(s1);
        studenten.add(s2);
        studenten.add(s3);
        System.out.println("\nUnsortiert: ");
        for(Student s : studenten) {
            System.out.println(s);
        }
        System.out.println("Sortiert: ");
        studenten.sort(null);
        for(Student s : studenten) {
            System.out.println(s);
        }
    }
}
