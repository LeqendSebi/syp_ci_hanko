package org.example;


import java.util.Objects;

public class PhoneBookEntry implements Comparable {

    private String number;
    private String name;

    public PhoneBookEntry(String name, String number) {
        if(correctNumber(number) && name != null && !name.trim().isEmpty()) {
            this.name = name;
            this.number = number;
        }
        else {
            throw new IllegalArgumentException("Number or Name incorrect");
        }


    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneBookEntry that = (PhoneBookEntry) o;
        return Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        return "Name: " + this.name + "; Nummer: " + this.number;
    }

    public boolean correctNumber(String numb) {
        try {

            if(numb.charAt(0) == '0') {
                long x = Long.parseLong(numb);
                return !numb.startsWith("000");

            }
            else if(numb.charAt(0) == '+' && numb.charAt(1) != '0') {
                long x = Long.parseLong(numb.substring(1));
                return true;


            }
            return false;

        }

        catch(Exception e) {
            return false;

        }
    }


    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        if (o == null || getClass() != o.getClass()) {
            throw new IllegalArgumentException("Nicht von Typ PhonebookEntry!");
        }

        PhoneBookEntry otherEntry = (PhoneBookEntry) o;

        int equal = this.name.compareTo(otherEntry.name);
        if (equal != 0) {
            return equal;
        }

        return otherEntry.number.compareTo(this.number);
    }
}
